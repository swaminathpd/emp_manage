package com.springboot.demo.entity;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Customer {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer custid;
	private String custno;
	private String custname;
	private float custsalary;
	public Integer getCustid() {
		return custid;
	}
	public void setCustid(Integer custid) {
		this.custid = custid;
	}
	public String getCustno() {
		return custno;
	}
	public void setCustno(String custno) {
		this.custno = custno;
	}
	public String getCustname() {
		return custname;
	}
	public void setCustname(String custname) {
		this.custname = custname;
	}
	public float getCustsalary() {
		return custsalary;
	}
	public void setCustsalary(float custsalary) {
		this.custsalary = custsalary;
	}
	@Override
	public int hashCode() {
		return Objects.hash(custid, custname, custno, custsalary);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		return Objects.equals(custid, other.custid) && Objects.equals(custname, other.custname)
				&& Objects.equals(custno, other.custno)
				&& Float.floatToIntBits(custsalary) == Float.floatToIntBits(other.custsalary);
	}
	
}

	