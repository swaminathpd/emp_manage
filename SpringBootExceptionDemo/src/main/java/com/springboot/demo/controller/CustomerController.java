package com.springboot.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.springboot.demo.dto.CustomerDto;
import com.springboot.demo.entity.Customer;
import com.springboot.demo.service.CustomerService;

@RestController
public class CustomerController {
	
@Autowired
private CustomerService customerService;


@RequestMapping("/hi")
public String sayHello() {
	return "Hello";
}
@RequestMapping("/customers")
public List<CustomerDto> getCustomerDetails(){
	
	return customerService.getCustomerDetails();
}

@PostMapping("/customers/save")
public Customer saveCustomerDetails(@RequestBody CustomerDto customerDto) {
	 return customerService.saveCustomerDetails(customerDto);
}

@PutMapping("/customers/{id}")
public Customer updateCustomerDeatils(@RequestBody CustomerDto customerDto, @PathVariable("id") Integer cid) {
	return customerService.updateCustomerDetails(customerDto, cid);
}
@DeleteMapping("/customers/delete/{id}")
public void deleteCustomer(@PathVariable("id") Integer cid) {
	customerService.deleteCustomer(cid);
	
}

}
