package com.springboot.demo.dto;

public class CustomerDto {
	private Integer custid;
	private String custno;
	private String custname;
	private float custsalary;
	public Integer getCustid() {
		return custid;
	}
	public void setCustid(Integer custid) {
		this.custid = custid;
	}
	public String getCustno() {
		return custno;
	}
	public void setCustno(String custno) {
		this.custno = custno;
	}
	public String getCustname() {
		return custname;
	}
	public void setCustname(String custname) {
		this.custname = custname;
	}
	public float getCustsalary() {
		return custsalary;
	}
	public void setCustsalary(float custsalary) {
		this.custsalary = custsalary;
	}
	

}
