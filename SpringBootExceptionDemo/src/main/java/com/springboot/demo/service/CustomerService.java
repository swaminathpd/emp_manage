package com.springboot.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.springboot.demo.dao.CustomerRepository;
import com.springboot.demo.dto.CustomerDto;
import com.springboot.demo.entity.Customer;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public List<CustomerDto> getCustomerDetails(){
		List<Customer> customers = new ArrayList<Customer>();
		List<CustomerDto> customerDtos = new ArrayList<CustomerDto>();
		customers=customerRepository.findAll();
		for (Customer customer : customers) {
			CustomerDto customerDto= new CustomerDto();
			customerDto.setCustid(customer.getCustid());
			customerDto.setCustno(customer.getCustno());
			customerDto.setCustname(customer.getCustname());
			customerDto.setCustsalary(customer.getCustsalary());
			customerDtos.add(customerDto);		
		}
		return customerDtos;
	}
	public Customer saveCustomerDetails(CustomerDto customerDto) {
		Customer customer = new Customer();
		customer.setCustno(customerDto.getCustno());
		customer.setCustname(customerDto.getCustname());
		customer.setCustsalary(customerDto.getCustsalary());
		return customerRepository.save(customer);
	}
  public Customer updateCustomerDetails(CustomerDto customerDto,Integer cid) {
	  Customer customer =customerRepository.findById(cid).get();
	  customer.setCustsalary(customerDto.getCustsalary());
	  return customerRepository.save(customer);	  
  }
  public void deleteCustomer(Integer cid) {
	  customerRepository.deleteById(cid);
  }
}
